# Scale Dependent Node Roles

In this repo you can find the code used in the experiments that were presented in our paper: "Local, Global and scale-dependent node roles". Which can be found here: <https://arxiv.org/abs/2105.12598> 

The repo consist of two python files: models.py, which defines the models used in the paper, and crossvalidation.py, which performs a hyperparameter search and crossvalidation over a given dataset. Additionally, there is a jupyter notebook for the extraction of crossvalidation results. 

## Installation


#### Pytorch
```
pip install torch==1.7.1+cu101 torchvision==0.8.2+cu101 torchaudio==0.7.2 -f https://download.pytorch.org/whl/torch_stable.html
```
#### Pytorch geometric
```
pip install torch-scatter==2.0.7 -f https://pytorch-geometric.com/whl/torch-1.7.1+cu101.html
pip install torch-sparse==0.6.8 -f https://pytorch-geometric.com/whl/torch-1.7.1+cu101.html
pip install torch-geometric
```
#### Pytorch lightning
```
pip install pytorch_lightning
```
#### Ray tune
```
pip install ray[tune]==1.2.0
```
#### Tensorflow (for extracting results)
```
pip install tensorflow
```
It is worth to note, that deprecation warnings may arise when using newer versions of pytorch and pytorch_geometric.

## Using the crossvalidation:
To use the cross validation type:

```
python crossvalidation.py [Module_name] [Dataset_name] [num_target] [size_embedding] [num_epochs] [num_workers]
```
where:
- Module_name is the name of the Module ([sortedNPNet_Module, Global_sortedNPNet_Module, GIN_Module, Gloabl_GIN_Module, Global_GIN_Module-])
- Dataset_name is the name of the dataset as stated in the TU-dataset importation of pytorch_geometric (see <https://pytorch-geometric.readthedocs.io/en/latest/modules/datasets.html#torch_geometric.datasets.TUDataset>)
- num_target is the number of target classes, or equivalently the size of the target vector. 
- size_embedding is the size of the embedding. Keep in mind, that this should be larger than $|V|$ for the SNPNet
- num_epochs is the number of epochs that the network is run
- num_workers is the number of threads that are used when executing hyperparameter tuning and cross validation in parallel. 

An example usage is as follows:
```
python crossvalidation.py sortedNPNet_Module MUTAG 7 28 100 32
```
This will execute a hyperparameter search and then a crossvalidation with the best hyperparameter combination found. Results of the hyperparameter search are written to memory under "tune_hyperparameter_search" and results of the crossvalidation are written to disk under "lightning_logs/crossvalidation". The crossvalidation results can be extracted using "Tensorboard" for nice visualisations or using the provided jupyter notebook. 

## Recreating the figures:
To recreate the figures, take a look at the "recreate_figures.ipynb" and the "main.py" files. 