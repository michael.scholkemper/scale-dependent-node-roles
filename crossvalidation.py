from models import GIN_Module, Global_Module, sortedNPNet_Module
import torch
import numpy as np
import torch_geometric
import networkx as nx
from torch.multiprocessing import Pool,  set_start_method
from pytorch_lightning.loggers import TensorBoardLogger
from torch.nn import  Sequential, Linear, ReLU,  Softmax, CrossEntropyLoss
from torch.optim import Adam 
from torch_geometric.data import DataLoader
from torch_geometric.nn import global_add_pool, GINConv
from pytorch_lightning import LightningModule, Trainer
from pytorch_lightning.metrics import Metric, Precision
from pytorch_lightning.metrics.utils import to_categorical
from ray.tune.integration.pytorch_lightning import TuneReportCallback
import torch.nn.functional as F
import ray.tune as tune
import sys
from models import GIN_Module, sortedNPNet_Module, Global_Module


# Function to compute the SNP-Embeddings
# Not part of the model as recomputing the embeddings every time is unneccesary during training. 
# The embeddings are precomputed once and extend the dataset. 
# The function gets nx.Graph, returns sorted-NP-node embeddings
def sortedNPEmbeddings(graph, embedding_size, num_layers):
    # compute adjacency matrix powers and concatenate them
    embeddings = torch.eye(len(graph), m=embedding_size, dtype=torch.float)
    embeddings_of_every_round = [embeddings.detach().clone().cpu().numpy()]
    scipy_adj = nx.convert_matrix.to_scipy_sparse_matrix(graph, format='coo')
    i = torch.tensor(np.vstack((scipy_adj.row, scipy_adj.col)))
    v = torch.tensor(scipy_adj.data)
    adj = torch.sparse_coo_tensor(i, v, dtype=torch.float, size=(len(graph), len(graph)))
    for i in range(num_layers):
        embeddings = torch.sparse.mm(adj, embeddings)
        embeddings_of_every_round.append(embeddings.detach().clone().cpu().numpy())
    
    embeddings_of_every_round = np.flip(np.array(embeddings_of_every_round), axis=0)
    sorted_embeddings_of_every_round = []
    #sort embeddings (quicker on cpu for some reason)
    for node in graph.nodes():
        ind = np.lexsort(embeddings_of_every_round[:,node,:])
        sorted_embeddings_of_every_round.append(np.flip(embeddings_of_every_round[:,node,ind],axis=0).flatten())
         
    return torch.from_numpy(np.array(sorted_embeddings_of_every_round))





# Main crossvalidation loop
# This is used as the training loop for the hyperparameter search and the actual crossvalidation. 
# It accepts various parameters related to the train loop. Apart from config, which is a dictionary of model hyperparameters such as learning rate, 
# the names are self-explanatory. 
# Output is printed to the terminal and saved under "tune_hyperparameter_search"
def crossvalidate_dataset(module_name, config, dataset_name, dataset_output_size, dataset_embedding_size, num_epochs, cv_fold, perm, num_folds=10):
    num_layers = config['num_layers']
    dataset_pyT = torch_geometric.datasets.TUDataset('~/scholkemper/pytorch_datasets', dataset_name)
    dataset_pyT = list(dataset_pyT)

    # Randomly permute the dataset (as some can be sorted and this would affect learning). This is consistent over all trials of the crossvalidation.
    dataset_pyT = [dataset_pyT[perm[i]] for i in range(len(dataset_pyT))]
    embedding_size = dataset_embedding_size
    out_size = dataset_output_size

    # If needed, compute SNP-Embeddings
    if module_name == 'sortedNPNet_Module' or module_name == 'Global_sortedNPNet_Module':
        for d in range(len(dataset_pyT)):
            dataset_pyT[d].snp_embeddings = sortedNPEmbeddings(torch_geometric.utils.to_networkx(dataset_pyT[d]), embedding_size, num_layers)

    # If the GNN is uninformed, denoted "-" in the paper, instead initialize the GNN with a constant vector. 
    if module_name[-1] == '-':
        for d in range(len(dataset_pyT)):
            dataset_pyT[d].x = torch.ones_like(dataset_pyT[d].x)

    # For the node-level task, the GNN must be uninformed as the targets "y" are set to the features of the nodes.
    if module_name[:6] != 'Global':
        for d in range(len(dataset_pyT)):
            dataset_pyT[d].y = to_categorical(dataset_pyT[d].x, argmax_dim=1)
            dataset_pyT[d].x = torch.ones_like(dataset_pyT[d].x)

    # Cut the data set into the folds for cross validation
    lower_bound = int(cv_fold/num_folds*len(dataset_pyT)) 
    upper_bound = int((cv_fold+1)/num_folds*len(dataset_pyT))
    train_dataset = dataset_pyT[:lower_bound]
    train_dataset.extend(dataset_pyT[upper_bound:])
    test_dataset = dataset_pyT[lower_bound:upper_bound]

    # Define train and test sets and the underlying MLP Modules for the GNNs
    train_loader = DataLoader(train_dataset, batch_size=20, shuffle=True, pin_memory=True)
    val_loader = DataLoader(test_dataset, batch_size=20, pin_memory=True)
    out = Sequential(
                Linear((num_layers+1)*embedding_size if module_name == 'sortedNPNet_Module' or module_name == 'Global_sortedNPNet_Module' else embedding_size, 500),
                ReLU(),
                Linear(500, 500),
                ReLU(),
                Linear(500, embedding_size if module_name[:6] == 'Global' else out_size)
            )
    gl_out = Sequential(
                Linear(embedding_size, 500),
                ReLU(),
                Linear(500, 500),
                ReLU(),
                Linear(500, out_size)
            )
    # Define the GNN configuration depending on the module that is to be run.
    if module_name == 'sortedNPNet_Module':
        module = sortedNPNet_Module(config, out_size, embedding_size=embedding_size, out_classifier=out)
    elif module_name == 'GIN_Module':
        module = GIN_Module(config, out_size, embedding_size=embedding_size, out_classifier=out)
    elif module_name == 'Global_sortedNPNet_Module':
        local_module = sortedNPNet_Module(config, out_size, embedding_size=embedding_size, out_classifier=out)
        module = Global_Module(config, local_module, out_size, embedding_size=embedding_size, gl_out_classifier=gl_out)
    elif module_name == 'Global_GIN_Module-' if module_name[-1] == '-' else 'Global_GIN_Module':
        local_module = GIN_Module(config, out_size, embedding_size=embedding_size, out_classifier=out)
        module = Global_Module(config, local_module, out_size, embedding_size=embedding_size, gl_out_classifier=gl_out)

    # Define ray.tune and pytorch_lightning callbacks for logging
    logger = TensorBoardLogger('lightning_logs/crossvalidation/test', name=module_name+'_'+dataset_name+'/nl'+str(num_layers)+'fold'+str(cv_fold))
    metrics = {'val_loss':'val_loss', 'train_acc':'train_acc_epoch', 'val_acc':'val_acc'}
    callbacks= [TuneReportCallback(metrics, on='validation_end')]
    trainer = Trainer(gpus=1, min_epochs=num_epochs, max_epochs=num_epochs, logger=logger, progress_bar_refresh_rate=0, callbacks=callbacks)
    logger.log_hyperparams({'embedding_size':embedding_size, 'lr':module.learning_rate, 'reg':module.regularization, 'n_layers':num_layers, 'init':'np_init', 'module':module_name})
    trainer.fit(module, train_loader, val_loader)



# Call for hyperparameter search using ray.tune, calls the crossvalidation loop on the first fold.           
def hyperparameter_search(config, module_name=None, dataset_name=None, dataset_output_size=2, dataset_embedding_size=32, num_epochs=500, perm=None, num_folds=10):
    crossvalidate_dataset(module_name, config, dataset_name, dataset_output_size, dataset_embedding_size, num_epochs, 1, perm, num_folds=num_folds)




if __name__ == '__main__':
    set_start_method('spawn')
    # Parse command line arguments 
    module_name = sys.argv[1] # Module_name
    dataset_name = sys.argv[2] # Dataset_name
    dataset_output_size = int(sys.argv[3]) # Number of target classes
    dataset_embedding_size = int(sys.argv[4]) # Size of embedding (Must be at least max |V|).
    num_epochs = int(sys.argv[5]) 
    num_workers = int(sys.argv[6]) # For parallel computing
    #permute the dataset randomly, since some may be sorted. 
    perm = np.random.permutation(len(list(torch_geometric.datasets.TUDataset('~/scholkemper/pytorch_datasets', dataset_name))))
    # config is a dictionary of hyperparameters of the models. The search for learning rate and regularization consists of sampling log-uniformly
    # from [0.0000001, 0.1]. The SNPNet is only allowed 3-5 layers whereas the GIN it allowed to be deeper. 
    config={'learning_rate': tune.loguniform(1e-7, 1e-1), 
            'regularization': tune.loguniform(1e-7, 1e-1), 
            'num_layers': (tune.grid_search([3,4,5]) if module_name[-18:] == 'sortedNPNet_Module' else tune.grid_search([3,4,5,6,7,8,9]))}
    analysis=tune.run(tune.with_parameters(hyperparameter_search, module_name=module_name, dataset_name=dataset_name, dataset_output_size=dataset_output_size, dataset_embedding_size=dataset_embedding_size, num_epochs=num_epochs, perm=perm, num_folds=3), 
                     metric='val_acc', mode='max', config=config, local_dir='tune_hyperparameter_search', name='tune_'+module_name, resources_per_trial={'gpu':1/num_workers}, num_samples=100, verbose=1, progress_reporter=tune.CLIReporter(print_intermediate_tables=True))
    #hyperparameter_search(config, module_name=module_name, dataset_name=dataset_name, dataset_output_size=dataset_output_size, dataset_embedding_size=dataset_embedding_size, num_epochs=num_epochs, perm=perm, num_folds=3)
    print(analysis.best_config)

    # Crossvalidation with found hyperparameters.
    with Pool(num_workers) as p:
       p.starmap(crossvalidate_dataset, [(module_name, analysis.best_config, dataset_name, dataset_output_size, dataset_embedding_size, num_epochs, i, perm) for i in range(10) for trials in range(10)])


            






