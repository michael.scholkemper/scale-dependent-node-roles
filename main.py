import networkx as nx
import numpy as np

from scipy import sparse

import copy

# implements an injective hash_function as a dictionary. Calling apply returns either the value present in the dict, or creates a new
# entry in the dict and return that. 
class HashFunction:
    def __init__(self):
        self.hash_dict = {}
        self.hash_counter = 2

    def apply(self, value):
        if value not in self.hash_dict:
            self.hash_dict[value] = self.hash_counter
            self.hash_counter += 1
        return self.hash_dict[value]


wl_hash = HashFunction()
small_gamma_hash = HashFunction()
big_gamma_hash = HashFunction()
structural_hash = HashFunction()

# Test whether the colorings are equivalent (which is used for early stopping in the weisfeiler-lehman algorithm)
def is_equivalent(coloring1, coloring2):
    col1 = sorted(coloring1)
    col2 = sorted(coloring2)
    last_value1 = col1[0]
    last_value2 = col2[0]
    for i in range(len(col1)):
        if col1[i] == last_value1 and col2[i] != last_value2:
            return False
        elif col1[i] != last_value1:
            if col2[i] == last_value2:
                return False
            else:
                last_value1 = col1[i]
                last_value2 = col2[i]
    return True


def weisfeiler_lehman(graph1: nx.Graph, iterations=-1, early_stopping=True, hash=wl_hash):
    if iterations == -1:
        iterations = len(graph1)

    # itialization of the colors
    Gamma1 = np.ones(len(graph1), dtype=np.int)
    set_colors_by_iteration = []
    colors_by_iteration = []

    for t in range(iterations):
        # copy colors since update is synchronous
        tmp_Gamma1 = np.copy(Gamma1)
        colors_by_iteration.append(copy.deepcopy(Gamma1))
        set_colors_by_iteration.append(set(Gamma1))

        # for each node compute the new color by hashing over the old color and the colors of neighbours (the sorted tuple emulates a mutliset)
        for node in range(len(graph1)):
            Gamma1[node] = hash.apply((Gamma1[node], tuple(sorted([tmp_Gamma1[n] for n in graph1[node]]))))

        # early stop if this is desired and the previous coloring and the current one are equivalent
        if is_equivalent(Gamma1, tmp_Gamma1) and early_stopping:
            return tmp_Gamma1, t, set_colors_by_iteration, colors_by_iteration

    colors_by_iteration.append(copy.deepcopy(Gamma1))
    set_colors_by_iteration.append(set(Gamma1))
    return Gamma1, iterations, set_colors_by_iteration, colors_by_iteration

# The implementation is different and more akin to the weisfeiler-lehman algorithm
def np_refinement(graph1, iterations=-1, gamma_hash=small_gamma_hash, Gamma_hash=big_gamma_hash):
    if iterations == -1:
        iterations = len(graph1)

    adjacency_matrix1 = nx.to_scipy_sparse_matrix(graph1, dtype=np.uint)
    # intialize matrices
    f1 = sparse.eye(len(graph1), dtype=np.ulonglong)
    gamma1 = {}
    Gamma1 = np.zeros(len(graph1), dtype=np.int)

    gamma_acc = [copy.deepcopy(gamma1)]
    Gamma_acc = [copy.deepcopy(Gamma1)]

    for t in range(iterations):
        # compute feature matrix, where each row in f1 is the feature of the node connected to that row
        f1 = adjacency_matrix1.dot(f1)
        zero_value = gamma_hash.hash_counter
        gamma_hash.hash_counter += 1
        # get non-zero values in f1
        x, y, value = sparse.find(f1)
        # compute a representation for each non-zero component of f1. This representation is unique for
        # each unique history of any component in the matrix. E.g. a component in the f1 matrix that was
        # 0 in the first matrix power and 1 in the second will have a different representation than one
        # that was 1 in the first and 0 in the second. Ordering the vector by this representation then
        # also determines the position of every column, just like a lex-sort would.
        for i in range(len(x)):
            if x[i] not in gamma1 :
                gamma1[x[i]] = {}
            gamma1[x[i]][y[i]] = gamma_hash.apply((gamma1[x[i]][y[i]], value[i])) if y[i] in gamma1[x[i]] else gamma_hash.apply((zero_value, value[i]))

        # Apply an injectiv hash_function on the sorted non-zero components of the previously computed 
        # representations of the non-zero values of f1.
        for node in range(len(graph1)):
            Gamma1[node] = Gamma_hash.apply((Gamma1[node], str(sorted(gamma1[node].values() if node in gamma1 else []))))

        gamma_acc.append(copy.deepcopy(gamma1))
        Gamma_acc.append(copy.deepcopy(Gamma1))
    return Gamma1, Gamma_acc, gamma_acc